/*
  Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
  Wed May 24 00:30:10 BRT 2017  
*/
#include <stdio.h>
 
int main(void)
{
    printf("Aspas duplas \" \n");
    printf("Barra: \\ \n");
    printf("Antes do \\t \t Depois do \\t\n");
    printf("Eu tenho %d anos\n", 37);
    return 0;
}