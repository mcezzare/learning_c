/*
  Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
  Thu May 25 01:17:52 BRT 2017
*/
#include <stdio.h>

int main()
{
    float pi = 3.14;
    double piDouble = 3.1415926535897932384626433832795;
    printf("Valor de pi %f\n", pi );
    printf("Valor de pi com 5 casas %.5f\n", pi );
    printf("Valor de pi mais preciso %f\n", piDouble );
    printf("Valor de pi mais preciso  com mais casas %.7f\n", piDouble );
    return 0;
}
