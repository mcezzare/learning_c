/*
  Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
  Thu May 25 01:30:14 BRT 2017
*/
#include <stdio.h>
int main()
{
    float salarioSonho = 1E6,
           salarioReal = 10E-3;
    printf("Sonhei que meu salario era de R$%.2f, \nmas acordei e lembrei que era %.2f centavos\n", salarioSonho, salarioReal);
    return 0;
}