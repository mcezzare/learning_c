/*
	Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
	Fri May 26 00:22:01 BRT 2017
*/
#include <stdio.h>

int main()
{
	char letra0 = 'C',
	letra1 = ' ',
	letra2 = 'P',
	letra3 = 'r',
	letra4 = 'o',
	letra5 = 'g',
	letra6 = 'r',
	letra7 = 'e',
	letra8 = 's',
	letra9 = 'i',
	letra10 = 'v',
	letra11 = 'o',
	letra12 = '\n';
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c\n",
	 letra0,
	 letra1, letra2, letra3, letra4,
	 letra5, letra6, letra7, letra8, letra8, letra9, letra10, letra11, letra12);
	return 0;
}
