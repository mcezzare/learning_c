/**
 *   Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
 *   Mon May 29 01:33:23 BRT 2017
 */

#include <stdio.h>

// int main(int argc, char const *argv[])
int main()
{
	/* code */
	printf("char:\t%lu bytes of RAM\n" , sizeof(char)) ;
	printf("short: %lu bytes of RAM\n" , sizeof(short)) ;
	printf("int: %lu bytes of RAM\n" , sizeof(int)) ;
	printf("unsigned:\t%lu bytes of RAM\n" , sizeof(unsigned)) ;
	printf("long:\t%lu bytes of RAM\n" , sizeof(long)) ;
	printf("unsigned int:\t%lu bytes of RAM\n" , sizeof(unsigned int)) ;
	printf("long long:\t%lu bytes of RAM\n" , sizeof(long long)) ;
	printf("float:\t%lu bytes of RAM\n" , sizeof(float)) ;
	printf("double:\t%lu bytes of RAM\n" , sizeof(double)) ;
	printf("long double:\t%lu bytes of RAM\n" , sizeof(long double)) ;
	return 0;
}