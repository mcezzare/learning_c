/**
 *   Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
 *   Wed May 31 01:06:22 BRT 2017
 */

#include <stdio.h>

int main()
{
    // divisao com numeros inteiros,descarta as casas decimais.
    int numero1,numero2;
    printf("Digite o 1º numero: ");
    scanf("%d", &numero1);

	printf("Digite o 2º numero: ");
    scanf("%d", &numero2);


    printf("O primeiro numero digitado foi: %d\n", numero1);
    printf("O segundo numero digitado foi: %d\n", numero2);
    printf("A soma é: %d\n", numero1 + numero2);
    printf("A subtração é: %d\n", numero1 - numero2);
    printf("A multiplicação é: %d\n", numero1 * numero2);

    float int_divisao  = numero1 / numero2;
    printf("A divisão com inteiros é: %3f\n", int_divisao);
    float int_modulo = numero1 % numero2 ; // so se aplica a numeros inteiros
    printf("O resto da divisao com inteiros é: %2f\n", int_modulo);

    float numero3 = numero1;
    float numero4 = numero2;
	printf("A divisão com float é: %3f\n", numero3 / numero4);
    return 0;
}