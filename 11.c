/**
 *   Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
 *   Thu Jun  1 00:44:09 BRT 2017
 */
/*
Exercício: Faça um programa em C que peça dois números do tipo 
double ao usuário e mostre o resultado da divisão do primeiro pelo 
segundo e exiba esse resultado com 3 casas decimais.
 */
#include <stdio.h>

int main(){

	double num1,num2;

	printf("Digite um numero:\n");
	scanf("%lf" , &num1);

	printf("Digite um numero:\n");
	scanf("%lf" , &num2);

	double resultado = num1 / num2 ;
	printf( " %f / %f = %.3f\n" , num1,num2,resultado); 
	return 0;
}