/**
 *   Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
 *   Wed Jun 21 00:10:32 BRT 2017
 */

#include <stdio.h>
int main()
{

#define PI 3.1415
    const double piDouble = 3.1415926535897932384626433832795;
    int piInt = piDouble;
    printf("Valor de pi %7f\n", piDouble);
    printf("Valor de PI %7f\n", PI);
    printf("Valor de PI %d\n", piInt); // nao arredonda!!! extrai o nº inteiro apenas

    return 0;
}