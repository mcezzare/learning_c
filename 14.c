/**
 *   Mario Cezzare Angelicola Chiodi <mcezzare@gmail.com>  
 *   Wed Jun 21 00:22:17 BRT 2017
 */

#include <stdio.h>
int main()
{
int x = 10 , y = 10 ;
    printf("Inicio e voltando sempre p/ 10: y = %d  x = %d \n" , y , x);
    y = ++x ;
    printf("Incremento Pre: y = ++x | y = %d  x = %d \n" , y , x);

    x = 10 ; y = 10 ;
    y = x++ ;
    printf("Incremento Pos: y = x++ | y = %d  x = %d \n" , y , x);
    
    // atribuicao simplificada
    x = 10 ; y = 10 ;
    x+= y;
    printf("Atribuicao simplificada: x+= y | y = %d  x = %d \n" , y , x);
    y-= x;
    printf("Atribuicao simplificada: y-= x; | y = %d  x = %d \n" , y , x);

    return 0;
}