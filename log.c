// log.cpp; 
 
#include <stdlib.h>
#include <stdio.h>
#include "log.h"
 
int LogCreated = 0;
 
void Log (char *message)
{
	FILE *file;
 
	if (!LogCreated) {
		file = fopen(LOGFILE, "w");
		LogCreated = 1;
	}
	else		
		file = fopen(LOGFILE, "a");
		
	if (file == NULL) {
		if (LogCreated)
			LogCreated = 0;
		return;
	}
	else
	{
		fputs(message, file);
		fclose(file);
	}
 
	if (file)
		fclose(file);
}
 
void LogErr (char *message)
{
	Log(message);
	Log("\n");
	// SysShutdown();
}

int main(){

	printf("%s\n" , "Starting program...,");
	Log("Logando 1\n");
	Log("Logando 2\n");
	Log("Logando 3\n");



	return 0;
}