// log.h; the header file which defines Log(); and LogErr();
// 

#define LOGFILE	"gl.log"
 
extern int LogCreated;      
 
void Log (char *message);    
void LogErr (char *message);
